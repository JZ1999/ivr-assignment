MiddleWare Models
==================

These are models used in the mysql database to be used throughout the software.


Transactions
==============
.. autoclass:: api.models.Transaction
   :members:

Credit Cards
===============
.. autoclass:: api.models.CreditCard
   :members:

Strip Credit Cards
===================
.. autoclass:: api.models.StripeCreditCard
   :members:

Stripe Tokens
===============
.. autoclass:: api.models.Token
   :members:
