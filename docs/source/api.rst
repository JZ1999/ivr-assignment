Stripe Card Token MiddleWare
==================

In order to use the middleware make sure to add

.. code-block:: python

    MIDDLEWARE = [
    	......
        'api.middleware.IVRTransactionMiddleware',
        ......
    ]



**NOTE: This middleware is only taken to full effect when the request is of type POST, from a form and it has to be json data**

The date being sent in must have the following format:

.. code-block:: json
   
   {
    "trans_id": "321654351687",
    "exp_date": "1220",
    "cc_num": "4242424242424242",
    "cvv": "123"
   }

If one of them are missing, are wrongly formatted, are not in the stripe database or wrong type then the corresponding error will occur.


Test Transaction view
==============
.. automodule:: api.views
    :members:

Api Configuration Class
==============
.. automodule:: api.apps
    :members:

MiddleWare
==============
.. automodule:: api.middleware
    :members:

Serializers
==============
.. automodule:: api.serializers
    :members:

Signals
==============
.. automodule:: api.signals
    :members:

Utilities
==============
.. automodule:: api.utils
    :members:

