from datetime import datetime

from api.models import Token, StripeCreditCard
from api.utils import format_exp_date, mask_credit_card
from .models import CreditCard, Transaction
from rest_framework import serializers


class CreditCardSerializer(serializers.ModelSerializer):
    trans_id = serializers.CharField()
    exp_date = serializers.RegexField(regex=r'^\d{4}$')

    class Meta:
        model = CreditCard
        exclude = ('transaction', 'id')

    def save(self, data):
        """
        Formats data and either gets the corresponding object from the database or creates it.
        Does a search with the masked form of the cc_num and adds the Transaction to the CreditCard object.

        :param data: A dictionary with all the data needed to create a Transaction and CreditCard object
        :return: Created CreditCard object
        """
        date = format_exp_date(data['exp_date'])  # since year comes with only the last 2 digits, we must add 2000 to it
        trans = Transaction.objects.get_or_create(trans_id=data['trans_id'])[0]
        cc_num = mask_credit_card(data['cc_num'])
        cc = CreditCard.objects.get_or_create(cc_num=cc_num, cvv=data['cvv'], exp_date=date)[0]
        if trans not in cc.transaction.all():
            cc.transaction.add(trans)
        return cc


class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = "__all__"


class StripeCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = StripeCreditCard
        fields = "__all__"
