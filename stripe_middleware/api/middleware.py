from stripe.error import CardError, RateLimitError, InvalidRequestError, AuthenticationError

from api.utils import format_exp_date
from .serializers import CreditCardSerializer, StripeCardSerializer, TokenSerializer
from rest_framework.status import HTTP_402_PAYMENT_REQUIRED, HTTP_400_BAD_REQUEST
from django.http import JsonResponse
from django.conf import settings
import stripe
import logging

logger = logging.getLogger("middleware")
stripe.api_key = settings.STRIPE_SECRET_KEY


class IVRTransactionMiddleware:
    """
    The middleware class to help IVR make a card token using the Stripe Restful API.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def is_stripe_request(self, post):
        """

        :param post: request.POST
        :return: True if request.POST has any of the values that are needed for the middleware to take effect
        """
        boolean = "exp_date" in post or "cc_num" in post or "cvv" in post or "trans_id" in post
        return boolean

    def __call__(self, request):
        """
        :return: Evaulates if middleware needs to take effect then adds an attribute to request to
        have the stripe_token which contains the authentication token to make any transaction in which it
        is needed in the Stripe Restful API.

        Must be a POST method, must have all of the POST parameters (exp_date, cc_num, cvv and trans_id).
        The token will make an attempt to be created if the value of the parameters are correct then the
        token will be given to the request otherwise the corresponding exception will be returned.
        """
        stripe_token = None

        if request.method == "POST":
            if self.is_stripe_request(request.POST):
                serializer = CreditCardSerializer(data=request.POST)
                if serializer.is_valid():
                    date = format_exp_date(serializer.validated_data["exp_date"])
                    # The input exp_date is as follows mmyy 2 digits for the month
                    # and 2 for the year (the last 2 digits of the year) so it must be
                    # formatted in order to work with it.
                    try:
                        stripe_token = stripe.Token.create(
                            card={
                                'number': serializer.validated_data['cc_num'],
                                'exp_month': date.month,
                                'exp_year': date.year,
                                'cvc': serializer.validated_data['cvv'],
                            }
                        )

                        stripe_token = stripe_token.to_dict()
                        card = StripeCardSerializer(data=stripe_token["card"])
                        if card.is_valid():
                            card = card.save()
                            logger.info("Saved card")
                        else:
                            logger.debug(f"Card not valid: {card.errors}")
                        token_info = {
                            "card": card.pk,
                            "client_ip": stripe_token["client_ip"],
                            "created": stripe_token["created"],
                            "id": stripe_token["id"],
                            "livemode": stripe_token["livemode"],
                            "object": stripe_token["object"],
                            "type": stripe_token["type"],
                            "used": stripe_token["used"],
                        }
                        token = TokenSerializer(data=token_info)
                        if token.is_valid():
                            token.save()
                            logger.info(f"Saved token {token}")
                        else:
                            logger.debug(f"Token not valid: {token.errors}")

                    except CardError as error:
                        logging.debug(f'Payment information invalid: {str(error)}')
                        return JsonResponse({"detail": "Payment information was invalid", "errors": str(error)},
                                            status=HTTP_400_BAD_REQUEST)
                    except RateLimitError as error:
                        logging.debug(f'Too many requests made to the API too quickly: {str(error)}')
                        return JsonResponse(
                            {"detail": "Too many requests made to the API too quickly", "errors": str(error)},
                            status=HTTP_400_BAD_REQUEST)
                    except InvalidRequestError as error:
                        logging.debug(f'Invalid parameters were supplied to Stripe\'s API: {str(error)}')
                        return JsonResponse(
                            {"detail": "Invalid parameters were supplied to Stripe\'s API", "errors": str(error)},
                            status=HTTP_400_BAD_REQUEST)
                    except AuthenticationError as error:
                        logging.debug(
                            f'Authentication with Stripe\'s API failed (maybe you changed API keys recently): {str(error)}')
                        return JsonResponse(
                            {"detail": "Authentication with Stripe\'s API failed (maybe you changed API keys recently)",
                             "errors": str(error)},
                            status=HTTP_400_BAD_REQUEST)
                    credit_card = serializer.save(serializer.validated_data)
                    logging.debug(f'saved {credit_card}')
                else:
                    logging.debug(f'Payment information invalid: {str(serializer.errors)}')
                    return JsonResponse({"detail": "Payment information was invalid", "errors": serializer.errors},
                                        status=HTTP_402_PAYMENT_REQUIRED)
                stripe_token = stripe_token["id"]
            else:
                logging.info(f'POST was not through a form.')
                return JsonResponse({"detail": "The POST is not submitted through a form"}, status=HTTP_400_BAD_REQUEST)
        else:
            logging.debug(f'Request was not a POST {request}')
        # Note that this middleware always assign the token
        # just that if the middleware does not run correctly then
        # it's value is None
        setattr(request, "stripe_token", stripe_token)
        response = self.get_response(request)
        logging.debug(f'Response being sent: {response}')
        return response
