from django.apps import AppConfig


class ApiConfig(AppConfig):
    """
    This configuration helps run the django signals needed to mask the credit card number and cvv number.
    """
    name = 'api'

    def ready(self):
        import api.signals
