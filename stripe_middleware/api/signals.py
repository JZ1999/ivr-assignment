from api.utils import mask_credit_card, mask_cvv
from .models import CreditCard
from django.db.models.signals import pre_save
from django.dispatch import receiver


@receiver(pre_save, sender=CreditCard)
def save_profile(sender, instance, raw, using, update_fields, **kwargs):
    """
    Uses the instance of the CreditCard before saving it and makes a mask, following PCI Compliance (CVV as well)
    """
    instance.cc_num = mask_credit_card(instance.cc_num)
    instance.cvv = mask_cvv(instance.cvv)
