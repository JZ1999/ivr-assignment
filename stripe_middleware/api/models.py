from django.core.validators import RegexValidator
from django.db import models


class Transaction(models.Model):
    """
    Saves the transaction id (primary key) of a request.
    """
    trans_id_message = "Invalid transaction id format, must be 12 characters long and only digits"
    trans_id_validator = RegexValidator(regex=r"^\d*$",
                                        message=trans_id_message)
    trans_id = models.CharField("Transaction ID", max_length=12, validators=[trans_id_validator], primary_key=True)

    def __str__(self):
        return f'Transaction #{self.trans_id}'


class CreditCard(models.Model):
    """
    Saves the credit card object from the request. Contains all the info that is being sent to the request.
    """
    cc_num_message = "Invalid credit card number, must be only numbers with no spaces"
    cc_num_validator = RegexValidator(regex=r"^\d*$",
                                      message=cc_num_message)
    cc_num = models.CharField("Credit Card", validators=[cc_num_validator], max_length=16, null=False)
    cvv_message = "Invalid CVV, must be 4 characters maximum long and only digits"
    cvv_validator = RegexValidator(regex=r"^\d*$",
                                   message=cvv_message)
    cvv = models.CharField("CVV", validators=[cvv_validator], max_length=4, null=False)
    exp_date = models.DateField("Expiration Date", null=False)
    transaction = models.ManyToManyField(Transaction)

    def __str__(self):
        return f'CC -> {self.cc_num} EXP DATE -> {self.exp_date}'


# The following models are for administration purposes, to save them in the database but have
# no functionality
class StripeCreditCard(models.Model):
    """
    After retrieving the card token information, even though only the token_id is given to the user through the request
    with the name stripe_token, the other information gathered is saved in the mysql database.
    """
    address_city = models.CharField(max_length=50, null=True, blank=True)
    address_country = models.CharField(max_length=50, null=True, blank=True)
    address_line1 = models.CharField(max_length=50, null=True, blank=True)
    address_line1_check = models.CharField(max_length=50, null=True, blank=True)
    address_zip = models.CharField(max_length=50, null=True, blank=True)
    address_zip_check = models.CharField(max_length=50, null=True, blank=True)
    brand = models.CharField(max_length=50, null=True, blank=True)
    country = models.CharField(max_length=50, null=True, blank=True)
    cvc_check = models.CharField(max_length=50, null=True, blank=True)
    dynamic_last4 = models.CharField(max_length=50, null=True, blank=True)
    exp_month = models.PositiveIntegerField(null=True, blank=True)
    exp_year = models.PositiveIntegerField(null=True, blank=True)
    funding = models.CharField(max_length=50, null=True, blank=True)
    id = models.CharField(max_length=50, primary_key=True)
    last4 = models.CharField(max_length=50, null=True, blank=True)
    name = models.CharField(max_length=50, null=True, blank=True)
    object = models.CharField(max_length=50, null=True, blank=True)
    tokenization_method = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return f"Stripe {self.pk}"


class Token(models.Model):
    """
    Token information gathered from the Stripe API response.
    """
    card = models.ForeignKey(StripeCreditCard, on_delete=models.CASCADE)
    client_ip = models.CharField(max_length=50)
    created = models.IntegerField()
    id = models.CharField(max_length=50, primary_key=True)
    livemode = models.BooleanField()
    object = models.CharField(max_length=50)
    type = models.CharField(max_length=50)
    used = models.BooleanField()

    def __str__(self):
        return f"Token  {self.pk}"
