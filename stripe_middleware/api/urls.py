from django.urls import path
from .views import Transaction

urlpatterns = [
    path('transaction/', Transaction.as_view(), name="transaction") # Test view, to see how the stripe_token is used
]
