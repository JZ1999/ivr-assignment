from django.http import HttpResponse
from rest_framework import generics
from .serializers import CreditCardSerializer


class Transaction(generics.GenericAPIView):
    """
    This class is used to demonstrate how the stripe_token is used
    """

    serializer_class = CreditCardSerializer

    def post(self, request):
        return HttpResponse(f"Your Stripe access token is {request.stripe_token}")
