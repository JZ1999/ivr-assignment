from django.contrib import admin
from .models import CreditCard, Transaction, Token, StripeCreditCard

# Registered for testing and debugging purposes
admin.site.register(CreditCard)
admin.site.register(Transaction)
admin.site.register(StripeCreditCard)
admin.site.register(Token)
