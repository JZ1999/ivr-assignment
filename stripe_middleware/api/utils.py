from datetime import datetime


def format_exp_date(exp_date: str):
    """

    :param exp_date: String in the format of mmyy
    :return: a tuple of corresponding date
    """
    month, year = int(exp_date[:2]), int(
        exp_date[2:]) + 2000
    return datetime(year, month, 1)


def mask_credit_card(credit_card: str):
    """
    Masks all digits up to the last 4
    :param credit_card: Credit card string in the format of 4242424242424242
    :return: masked version of credit card
    """
    credit_card_masked = credit_card[-4:].rjust(len(credit_card), "*")

    return credit_card_masked


def mask_cvv(cvv: str):
    """
    Masks all digits
    :param cvv: Credit card string in the format of 123
    :return: masked version of cvv
    """
    cvv_masked = "".join(map(lambda x: "*", cvv))
    return cvv_masked
