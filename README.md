# Stripe token middleware

Stripe middleware to easily make transactions in views with a card token. (Python 3.6+)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them. You will need to have mysql installed.

```
pip install -r requirements.txt
```

Make sure to create a .env to hold all your environment variables. you may look at the src/irv/.env.example for an example.

### Running
After creating a database for this proyect in mysql (By default it looks for a database called ivr).

```
python manage.py migrate
python manage.py runserver
```

You may want to create a super user to check out the saved data.

```
python manage.py createsuperuser
```

**Note:** Log files are saved in logs/debug_request.logs

## Usage

Look at the src/api/views.py for a simple example. The middleware makes sure to add a stripe_token to all requests, but it must
have correct data and must be a JSON POST method.

Example:

```
{
    "trans_id": "321654351687",
    "exp_date": "1220",
    "cc_num": "4242424242424242",
    "cvv": "123"
}
```

**Note:** The post must be in a form go to /api/transaction/ for an example.

## Documentation

The documentation was built using Sphinx. So in order to view it you must compile the source with the following commands:

```
cd docs
sphinx-build -b html source build
```

To view the documentation either open the index.html with an internet browser or run the following command and in
the internet browser go to http://localhost:9489

```
cd build
python -m http.server 9000
```

## Authors

* **Joseph Zamora** - [JZ1999](https://github.com/JZ1999)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

